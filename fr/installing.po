# translation of installing.po to French
# Translation of Debian release notes to French
# Copyright (C) 2005-2011 Debian French l10n team <debian-l10n-french@lists.debian.org>
# This file is distributed under the same license as the Debian release notes.
#
# Translators:
# Denis Barbier, -2004
# Frédéric Bothamy <frederic.bothamy@free.fr>, 2004-2007.
# Simon Paillard <simon.paillard@resel.enst-bretagne.fr>, 2008-2009.
# Christian Perrier <bubulle@debian.org>, 2009.
# Thomas Blein <tblein@tblein.eu>, 2011, 2012, 2015.
# Baptiste Jammet <baptiste@mailoo.org>, 2017, 2019, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: installing\n"
"POT-Creation-Date: 2023-03-24 10:48+0100\n"
"PO-Revision-Date: 2023-03-24 11:00+0100\n"
"Last-Translator: Baptiste Jammet <baptiste@mailoo.org>\n"
"Language-Team: French <debian-l10n-french@lists.debian.org>\n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 20.12.0\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#. type: Attribute 'lang' of: <chapter>
#: en/installing.dbk:8
msgid "en"
msgstr "fr"

#. type: Content of: <chapter><title>
#: en/installing.dbk:9
msgid "Installation System"
msgstr "Système d'installation"

#. type: Content of: <chapter><para>
#: en/installing.dbk:11
#| msgid ""
#| "The Debian Installer is the official installation system for Debian.  It "
#| "offers a variety of installation methods.  Which methods are available to "
#| "install your system depends on your architecture."
msgid ""
"The Debian Installer is the official installation system for Debian.  It "
"offers a variety of installation methods.  The methods that are available to "
"install your system depend on its architecture."
msgstr ""
"L'installateur Debian est le système officiel d'installation pour Debian. Il "
"offre plusieurs méthodes d'installation. Les méthodes disponibles pour "
"installer votre système dépendent de son architecture."

#. type: Content of: <chapter><para>
#: en/installing.dbk:16
msgid ""
"Images of the installer for &releasename; can be found together with the "
"Installation Guide on the <ulink url=\"&url-installer;\">Debian website</"
"ulink>."
msgstr ""
"Les images de l'installateur pour &Releasename;, ainsi que le manuel "
"d'installation, se trouvent sur le <ulink url=\"&url-installer;\">site web "
"de Debian</ulink>."

#. type: Content of: <chapter><para>
#: en/installing.dbk:21
msgid ""
"The Installation Guide is also included on the first media of the official "
"Debian DVD (CD/blu-ray) sets, at:"
msgstr ""
"Le manuel d'installation se trouve également sur le premier médium de "
"l'ensemble des DVD (ou CD ou Blu-ray) Debian officiels, dans :"

#. type: Content of: <chapter><screen>
#: en/installing.dbk:25
#, no-wrap
msgid "/doc/install/manual/<replaceable>language</replaceable>/index.html\n"
msgstr "/doc/install/manual/<replaceable>langue</replaceable>/index.html\n"

#. type: Content of: <chapter><para>
#: en/installing.dbk:28
msgid ""
"You may also want to check the <ulink url=\"&url-installer;index#errata"
"\">errata</ulink> for debian-installer for a list of known issues."
msgstr ""
"Il est également possible de consulter les <ulink url=\"&url-installer;"
"index#errata\">errata</ulink> de l'installateur Debian pour une liste de "
"problèmes connus."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:33
msgid "What's new in the installation system?"
msgstr "Quoi de neuf dans le système d'installation ?"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:35
msgid ""
"There has been a lot of development on the Debian Installer since its "
"previous official release with &debian; &oldrelease;, resulting in improved "
"hardware support and some exciting new features or improvements."
msgstr ""
"Depuis sa dernière publication officielle avec &debian; &oldrelease;, "
"l'installateur Debian a remarquablement évolué, offrant une meilleure prise "
"en charge du matériel et de nouvelles fonctionnalités ou des améliorations "
"très intéressantes."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:41
#| msgid ""
#| "If you are interested in an overview of the detailed changes since "
#| "&oldreleasename;, please check the release announcements for the "
#| "&releasename; beta and RC releases available from the Debian Installer's "
#| "<ulink url=\"&url-installer-news;\">news history</ulink>."
msgid ""
"If you are interested in an overview of the changes since &oldreleasename;, "
"please check the release announcements for the &releasename; beta and RC "
"releases available from the Debian Installer's <ulink url=\"&url-installer-"
"news;\">news history</ulink>."
msgstr ""
"Si vous êtes intéressé par un aperçu des changements depuis "
"&Oldreleasename;, veuillez consulter les annonces de publication pour les "
"versions bêta et candidates de &Releasename; dans l'<ulink url=\"&url-"
"installer-news;\">historique des nouveautés</ulink> du projet de "
"l'installateur Debian."

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:52
msgid "Something"
msgstr "Vide"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:54
msgid "Text"
msgstr "Texte vide"

#. type: Content of: <chapter><section><section><title>
#: en/installing.dbk:128
msgid "Automated installation"
msgstr "Installation automatisée"

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:130
msgid ""
"Some changes in this release are not fully backwards-compatible with "
"previous versions of preseeding, the process used for automatic installation."
msgstr ""
"Certains changements dans cette publication ne sont pas complètement "
"compatibles avec les anciennes versions de la préconfiguration, utilisée "
"pour les installations automatisées."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:133
msgid ""
"If you have existing preseed configuration settings that worked with the "
"&oldreleasename; installer, you should assume that modifications will be "
"required for them to work correctly with the &releasename; installer."
msgstr ""
"Si vous avez des paramètres de préconfiguration qui fonctionnaient avec "
"l'installateur de &Oldreleasename;, vous devriez considérer qu'ils "
"nécessitent des ajustements pour fonctionner correctement avec "
"l'installateur de &Releasename;."

#. type: Content of: <chapter><section><section><para>
#: en/installing.dbk:138
#| msgid ""
#| "The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> has an "
#| "updated separate appendix with extensive documentation on using "
#| "preconfiguration."
msgid ""
"The <ulink url=\"&url-install-manual;\">Installation Guide</ulink> contains "
"an appendix with extensive documentation on preseeding."
msgstr ""
"Le <ulink url=\"&url-install-manual;\">manuel d'installation</ulink> "
"comprend une annexe séparée avec une documentation complète sur "
"l'utilisation de la préconfiguration."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:146
msgid "Cloud installations"
msgstr "Installations pour l'informatique dématérialisée (« cloud »)"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:148
msgid ""
"The <ulink url=\"&url-cloud-team;\">cloud team</ulink> publishes Debian "
"&releasename; for several popular cloud computing services including:"
msgstr ""
"L'<ulink url=\"&url-cloud-team;\">équipe pour l'informatique dématérialisée</"
"ulink> publie des images de Debian &Releasename; pour différents services de "
"« cloud computing » populaires, dont :"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:155
msgid "OpenStack"
msgstr "OpenStack"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:160
msgid "Amazon Web Services"
msgstr "Amazon Web Services"

#. type: Content of: <chapter><section><para><itemizedlist><listitem><para>
#: en/installing.dbk:165
msgid "Microsoft Azure"
msgstr "Microsoft Azure"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:172
#| msgid ""
#| "Cloud images provide automation hooks via cloud-init and prioritize fast "
#| "instance startup using specifically optimized kernel packages and grub "
#| "configurations.  Images supporting different architectures are provided "
#| "where appropriate and the cloud team endeavors to support all features "
#| "offered by the cloud service."
msgid ""
"Cloud images provide automation hooks via <command>cloud-init</command> and "
"prioritize fast instance startup using specifically optimized kernel "
"packages and grub configurations.  Images supporting different architectures "
"are provided where appropriate and the cloud team endeavors to support all "
"features offered by the cloud service."
msgstr ""
"Les images pour l'informatique dématérialisée fournissent des accroches "
"(« hooks ») à l’aide de <command>cloud-init</command> et favorisent le "
"démarrage rapide des instances en utilisant des paquets de noyau optimisé et "
"des configurations de GRUB adaptées. Des images pour différentes "
"architectures sont disponibles, et l'équipe pour l'informatique "
"dématérialisée met tout en œuvre pour prendre en charge toutes les "
"fonctionnalités offertes par le fournisseur de services."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:181
msgid ""
"The cloud team will provide updated images until the end of the LTS period "
"for &releasename;. New images are typically released for each point release "
"and after security fixes for critical packages.  The cloud team's full "
"support policy can be found <ulink url=\"&url-cloud-wiki-imagelifecycle;"
"\">here</ulink>."
msgstr ""
"L'équipe pour l'informatique dématérialisée fournira des images à jour "
"jusqu'à la fin de la période de prise en charge à long terme (LTS) de "
"&Releasename;. Les nouvelles images sont généralement publiées à chaque "
"publication intermédiaire et après les corrections de sécurité pour les "
"paquets de grande importance. La charte complète de l'équipe pour "
"l'informatique dématérialisée est disponible à <ulink url=\"&url-cloud-wiki-"
"imagelifecycle;\">cette adresse</ulink>."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:189
msgid ""
"More details are available at <ulink url=\"&url-cloud;\">cloud.debian.org</"
"ulink> and <ulink url=\"&url-cloud-wiki;\">on the wiki</ulink>."
msgstr ""
"Plus de détails sont disponibles à l'adresse <ulink url=\"&url-cloud;"
"\">cloud.debian.org</ulink> et <ulink url=\"&url-cloud-wiki;\">sur le wiki</"
"ulink>."

#. type: Content of: <chapter><section><title>
#: en/installing.dbk:196
msgid "Container and Virtual Machine images"
msgstr "Images pour les conteneurs et les machines virtuelles"

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:198
msgid ""
"Multi-architecture Debian &releasename; container images are available on "
"<ulink url=\"&url-docker-hub;\">Docker Hub</ulink>.  In addition to the "
"standard images, a <quote>slim</quote> variant is available that reduces "
"disk usage."
msgstr ""
"Des images multi-architectures de Debian &Releasename; sont disponibles sur "
"<ulink url=\"&url-docker-hub;\">Docker Hub</ulink>. En plus des images "
"standards, une variante <quote>slim</quote>, plus petite, est disponible "
"pour minimiser l'espace disque."

#. type: Content of: <chapter><section><para>
#: en/installing.dbk:204
msgid ""
"Virtual machine images for the Hashicorp Vagrant VM manager are published to "
"<ulink url=\"&url-vagrant-cloud;\">Vagrant Cloud</ulink>."
msgstr ""
"Des images pour machines virtuelles adaptées au gestionnaire de machines "
"virtuelles Hashicorp Vagrant sont publiées sur <ulink url=\"&url-vagrant-"
"cloud;\">Vagrant Cloud</ulink>."

#~ msgid ""
#~ "Some changes also imply changes in the support in the installer for "
#~ "automated installation using preconfiguration files.  This means that if "
#~ "you have existing preconfiguration files that worked with the "
#~ "&oldreleasename; installer, you cannot expect these to work with the new "
#~ "installer without modification."
#~ msgstr ""
#~ "Certaines modifications impliquent également des changements dans la "
#~ "gestion des installations automatisées qui utilisent des fichiers de "
#~ "préconfiguration. Les fichiers de préconfiguration existants, qui "
#~ "fonctionnent avec l'installateur de &Oldreleasename;, ne fonctionneront "
#~ "pas sans modification avec le nouvel installateur."

#~| msgid "Help during the installation process"
#~ msgid "Help with installation of firmware"
#~ msgstr "Aide pour l'installation de microprogramme"

#~ msgid ""
#~ "More and more, peripheral devices require firmware to be loaded as part "
#~ "of the hardware initialization. To help deal with this problem, the "
#~ "installer has a new feature. If some of the installed hardware requires "
#~ "firmware files to be installed, the installer will try to add them to the "
#~ "system, based on a mapping from hardware ID to firmware file names."
#~ msgstr ""
#~ "De plus en plus de composants nécessitent le chargement d'un "
#~ "microprogramme lors de l'initialisation du matériel. Pour tenter de "
#~ "pallier à ce problème, l'installateur intègre une nouvelle "
#~ "fonctionnalité. Si un périphérique nécessite l'installation d'un "
#~ "microprogramme, l'installateur tentera de l'ajouter au système en se "
#~ "basant sur une correspondance entre l'identifiant du composant et le nom "
#~ "de fichier du microprogramme."

#~ msgid ""
#~ "This new functionality is restricted to the unofficial installer images "
#~ "with firmware included (see <ulink url=\"&url-installer;#firmware_nonfree"
#~ "\">&url-installer;#firmware_nonfree</ulink>).  The firmware is usually "
#~ "not DFSG compliant, so it is not possible to distribute it in Debian's "
#~ "main repository."
#~ msgstr ""
#~ "Cette nouvelle fonctionnalité est restreinte aux images non officielles "
#~ "qui incluent les microprogrammes (consultez la <ulink url=\"&url-"
#~ "installer;#firmware_nonfree\">&url-installer;#firmware_nonfree</ulink>). "
#~ "Le microprogramme n'est généralement pas compatible avec les principes du "
#~ "logiciel libre selon Debian, et il n'est donc pas possible de les "
#~ "distribuer dans le dépôt principal de Debian."

#~ msgid ""
#~ "If you experience problems related to (missing) firmware, please read "
#~ "<ulink url=\"https://www.debian.org/releases/bullseye/amd64/"
#~ "ch06s04#completing-installed-system\">the dedicated chapter of the "
#~ "installation-guide</ulink>."
#~ msgstr ""
#~ "Si vous rencontrez des problèmes liés à un (manque de) microprogramme, "
#~ "veuillez lire <ulink url=\"https://www.debian.org/releases/bullseye/amd64/"
#~ "ch06s04#completing-installed-system\">le chapitre dédié du manuel "
#~ "d'installation</ulink>."

#~ msgid ""
#~ "Most notably there is the initial support for UEFI Secure Boot (see <xref "
#~ "linkend=\"secure-boot\"/>), which has been added to the installation "
#~ "images."
#~ msgstr ""
#~ "Le plus important est la prise en charge initiale de l'amorçage sécurisé "
#~ "avec UEFI (« UEFI secure boot », consultez la <xref linkend=\"secure-boot"
#~ "\"/>) qui a été ajoutée aux images d'installation."

#~ msgid "Major changes"
#~ msgstr "Changements majeurs"

#~ msgid "Removed ports"
#~ msgstr "Portages retirés"

#~ msgid ""
#~ "Support for the <literal>powerpc</literal> architecture has been removed."
#~ msgstr ""
#~ "La prise en charge de l'architecture <literal>powerpc</literal> a été "
#~ "supprimée de l'installateur."

#~ msgid "New ports"
#~ msgstr "Nouveaux portages"

#~ msgid ""
#~ "Support for the <literal>mips64el</literal> architecture has been added "
#~ "to the installer."
#~ msgstr ""
#~ "La prise en charge de l'architecture <literal>mips64el</literal> a été "
#~ "ajoutée à l'installateur."

#~ msgid "Graphical installer"
#~ msgstr "Installateur graphique"

#~ msgid ""
#~ "The graphical installer is now the default on supported platforms.  The "
#~ "text installer is still accessible from the very first menu, or if the "
#~ "system has limited capabilities."
#~ msgstr ""
#~ "L'installateur graphique est maintenant l'installateur par défaut pour "
#~ "les plateformes prises en charge. L'installateur en mode texte reste "
#~ "accessible à partir du tout premier menu ou si le système a des capacités "
#~ "limitées."

#~ msgid "The kernel flavor has been bumped to <literal>i686</literal>"
#~ msgstr "La saveur du noyau a été passée à <literal>i686</literal>."

#~ msgid ""
#~ "The kernel flavor <literal>i586</literal> has been renamed to "
#~ "<literal>i686</literal>, since <literal>i586</literal> is no longer "
#~ "supported."
#~ msgstr ""
#~ "La saveur du noyau <literal>i586</literal> a été renommée en "
#~ "<literal>i686</literal> car <literal>i586</literal> n'est plus prise en "
#~ "charge."

#~ msgid "Desktop selection"
#~ msgstr "Sélection du bureau"

#~ msgid ""
#~ "Since jessie, the desktop can be chosen within tasksel during "
#~ "installation, and several desktops can be selected at the same time."
#~ msgstr ""
#~ "Depuis Jessie, l'environnement de bureau peut être choisi au sein de "
#~ "tasksel pendant l'installation, et plusieurs environnements de bureau "
#~ "peuvent être sélectionnés en même temps."

#~ msgid "New languages"
#~ msgstr "Nouvelles langues"

#~ msgid ""
#~ "Thanks to the huge efforts of translators, &debian; can now be installed "
#~ "in 75 languages, including English.  Most languages are available in both "
#~ "the text-based installation user interface and the graphical user "
#~ "interface, while some are only available in the graphical user interface."
#~ msgstr ""
#~ "Grâce à l'énorme travail des traducteurs, &debian; peut maintenant être "
#~ "installée dans 75 langues, dont le français. La plupart des langues sont "
#~ "disponibles dans les deux interfaces d'installation, textuelle et "
#~ "graphique, tandis que quelques-unes sont seulement disponibles dans "
#~ "l'interface utilisateur graphique."

#~ msgid ""
#~ "The languages that can only be selected using the graphical installer as "
#~ "their character sets cannot be presented in a non-graphical environment "
#~ "are: Amharic, Bengali, Dzongkha, Gujarati, Hindi, Georgian, Kannada, "
#~ "Khmer, Malayalam, Marathi, Nepali, Punjabi, Tamil, Telugu, Tibetan, and "
#~ "Uyghur."
#~ msgstr ""
#~ "Certaines langues ne peuvent être choisies qu'avec l'installateur "
#~ "graphique car leur jeu de caractères ne peut pas être représenté dans un "
#~ "environnement non graphique. Il s'agit des langues suivantes : "
#~ "l'amharique, le bengali, le dzongkha, le gujarati, l'hindi, le géorgien, "
#~ "le kannada, le khmer, le malayalam, le marathi, le népalais, le pendjabi, "
#~ "le tamoul, le télougou, le tibétain et le ouïghour."

#~ msgid "UEFI boot"
#~ msgstr "Démarrage UEFI"

#~ msgid ""
#~ "The &releasename; installer improves support for a lot of UEFI firmware "
#~ "and also supports installing on 32-bit UEFI firmware with a 64-bit kernel."
#~ msgstr ""
#~ "L'installateur de &Releasename; améliore la prise en charge pour beaucoup "
#~ "de micrologiciels UEFI et prend en charge l'installation sur un "
#~ "micrologiciel UEFI 32 bits avec un noyau 64 bits."

#~ msgid "Note that this does not include support for UEFI Secure Boot."
#~ msgstr ""
#~ "Veuillez noter que cela n'inclut pas la prise en charge du démarrage "
#~ "sécurisé d'UEFI (« Secure Boot »)."

#~ msgid "New method for naming network interfaces"
#~ msgstr "Nouvelle méthode de nommage des interfaces réseau"

#~ msgid ""
#~ "The installer and the installed systems use a new standard naming scheme "
#~ "for network interfaces.  <literal>ens0</literal> or <literal>enp1s1</"
#~ "literal> (ethernet)  or <literal>wlp3s0</literal> (wlan) will replace the "
#~ "legacy <literal>eth0</literal>, <literal>eth1</literal>, etc.  See <xref "
#~ "linkend=\"new-interface-names\"/> for more information."
#~ msgstr ""
#~ "L'installateur et les systèmes installés utilisent un nouveau schéma de "
#~ "nommage des interfaces réseau. Les noms <literal>ens0</literal> ou "
#~ "<literal>enp1s1</literal> (ethernet) ou <literal>wlp3s0</literal> (sans "
#~ "fil) remplacent les anciens <literal>eth0</literal>, <literal>eth1</"
#~ "literal>, etc. Consultez la <xref linkend=\"new-interface-names\"/> pour "
#~ "plus d'informations."

#~ msgid "Multi-arch images now default to <literal>amd64</literal>"
#~ msgstr "<literal>amd64</literal> par défaut dans les images multi-arch"

#~ msgid ""
#~ "Since 64-bit PCs have become more common, the default architecture on "
#~ "multi-arch images is now <literal>amd64</literal> instead of "
#~ "<literal>i386</literal>."
#~ msgstr ""
#~ "Puisque les PC 64 bits deviennent de plus en plus courants, "
#~ "l'architecture par défaut sur les images multi-arch est maintenant "
#~ "<literal>amd64</literal> à la place de <literal>i386</literal>."

#~ msgid "Full CD sets removed"
#~ msgstr "Jeux complets de CD retirés"

#~ msgid ""
#~ "The full CD sets are not built anymore. The DVD images are still "
#~ "available as well as the netinst CD image."
#~ msgstr ""
#~ "Les jeux complets de CD ne sont plus construits. Les images DVD, ainsi "
#~ "que les images d'installation par le réseau (« netinst ») sont toujours "
#~ "disponibles."

#~ msgid ""
#~ "Also, as the installer now gives an easy choice of desktop selection "
#~ "within tasksel, only Xfce CD#1 remains as a single-CD desktop system."
#~ msgstr ""
#~ "De plus, comme l'installateur permet de choisir facilement "
#~ "l'environnement bureau au sein de tasksel, seul le CD n°1 de Xfce est "
#~ "toujours disponible comme CD unique comportant un environnement de bureau."

#~ msgid "Accessibility in the installer and the installed system"
#~ msgstr "Accessibilité de l'installateur et du système installé"

#~ msgid ""
#~ "The installer produces two beeps instead of one when booted with grub, so "
#~ "users can tell that they have to use the grub method of editing entries."
#~ msgstr ""
#~ "L'installateur émet deux bips, au lieu d'un, lorsqu'il démarre avec grub. "
#~ "Ainsi, l'utilisateur sait qu'il doit utiliser la méthode grub pour éditer "
#~ "les entrées."

#~ msgid ""
#~ "MATE desktop is the default desktop when brltty or espeakup is used in "
#~ "debian-installer."
#~ msgstr ""
#~ "L'environnement de bureau MATE est celui par défaut lorsque brltty ou "
#~ "espeakup sont utilisés dans l'installateur."

#~ msgid "Added HTTPS support"
#~ msgstr "Ajout de la prise en charge HTTPS"

#~ msgid ""
#~ "Support for HTTPS has been added to the installer, enabling downloading "
#~ "of packages from HTTPS mirrors."
#~ msgstr ""
#~ "La prise en charge de HTTPS a été ajoutée à l'installateur, autorisant "
#~ "ainsi le téléchargement de paquets depuis des miroirs HTTPS."

#~ msgid ""
#~ "Support for the 'ia64' and 'sparc' architectures has been dropped from "
#~ "the installer since they have been removed from the archive."
#~ msgstr ""
#~ "La prise en charge des architectures « ia64 » et « sparc » a été "
#~ "supprimée de l'installateur car elles ont été retirées de l'archive."

#~ msgid "New default init system"
#~ msgstr "Nouveau système d'initialisation par défaut"

#~ msgid ""
#~ "The installation system now installs systemd as the default init system."
#~ msgstr ""
#~ "Le système d'installation installe maintenant « systemd » comme système "
#~ "d'initialisation par défaut."

#~ msgid "Replacing \"--\" by \"---\" for boot parameters"
#~ msgstr ""
#~ "Remplacement de « -- » par « --- » pour les paramètres de démarrage."

#~ msgid ""
#~ "Due to a change on the Linux kernel side, the \"---\" separator is now "
#~ "used instead of the historical \"--\" to separate kernel parameters from "
#~ "userland parameters."
#~ msgstr ""
#~ "À cause d'un changement du côté du noyau Linux, le séparateur « --- » est "
#~ "maintenant utilisé au lieu de l'historique « -- » pour séparer les "
#~ "paramètres du noyau de ceux de l'espace utilisateur."

#~ msgid "Languages added in this release:"
#~ msgstr "Voici les langues ajoutées à cette version :"

#~ msgid "Tajik has been added to the graphical and text-based installer."
#~ msgstr ""
#~ "le tadjik a été ajouté aux deux interfaces d'installation, textuelle et "
#~ "graphique"

#~ msgid ""
#~ "Welsh has been re-added to the graphical and text-based installer (it had "
#~ "been removed in &oldreleasename;)."
#~ msgstr ""
#~ "le gallois a été ajouté à nouveau à l'installeur graphique et textuel (il "
#~ "avait été supprimé de &Oldreleasename;) ;"

#~ msgid "Software speech support"
#~ msgstr "Prise en charge de logiciel de synthèse vocale."

#~ msgid ""
#~ "&debian; can be installed using software speech, for instance by visually "
#~ "impaired people who do not use a Braille device.  This is triggered "
#~ "simply by typing <literal>s</literal> and <literal>Enter</literal> at the "
#~ "installer boot beep.  More than a dozen languages are supported."
#~ msgstr ""
#~ "&debian; peut être installée en utilisant un logiciel de synthèse vocale, "
#~ "par exemple pour les personnes malvoyantes qui n'utilisent pas de "
#~ "périphérique braille. La synthèse vocale est activée simplement en tapant "
#~ "<literal>s</literal> et <literal>Entrer</literal> lors du beep de boot de "
#~ "l'installateur. Plus d'une douzaine de langues sont prises en charge."

#~ msgid "New supported platforms"
#~ msgstr "Nouvelles plateformes prises en charge."

#~ msgid "Intel Storage System SS4000-E"
#~ msgstr "système de stockage Intel SS4000-E ;"

#~ msgid "Marvell's Kirkwood platform:"
#~ msgstr "plateforme Kirkwood de Marvell :"

#~ msgid "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P and TS-419P"
#~ msgstr "QNAP TS-110, TS-119, TS-210, TS-219, TS-219P et TS-419P ;"

#~ msgid "Marvell SheevaPlug and GuruPlug"
#~ msgstr "SheevaPlug et GuruPlug de Marvell ;"

#~ msgid "Marvell OpenRD-Base, OpenRD-Client and OpenRD-Ultimate"
#~ msgstr "OpenRD-Base, OpenRD-Client et OpenRD-Ultimate de Marvell ;"

#~ msgid "HP t5325 Thin Client (partial support)"
#~ msgstr "client léger HP t5325 (prise en charge partielle)."

#~ msgid "Network configuration"
#~ msgstr "Configuration réseau"

#~ msgid "The installer now supports installation on IPv6-only networks."
#~ msgstr ""
#~ "L'installeur prend maintenant en charge l'installation sur les réseaux "
#~ "qui ne sont qu'en IPv6."

#~ msgid "It is now possible to install over a WPA-encrypted wireless network."
#~ msgstr ""
#~ "Il est maintenant possible d'installer à travers un réseau sans fil "
#~ "chiffré en WPA."

#~ msgid ""
#~ "<literal>ext4</literal> is the default filesystem for new installations, "
#~ "replacing <literal>ext3</literal>."
#~ msgstr ""
#~ "<literal>ext4</literal> est le système de fichiers par défaut pour les "
#~ "nouvelles installations et remplace <literal>ext3</literal>."

#~ msgid ""
#~ "The <literal>btrfs</literal> filesystem is provided as a technology "
#~ "preview."
#~ msgstr ""
#~ "Le système de fichiers <literal>btrfs</literal> est fourni en tant "
#~ "qu'aperçu technologique."

#~ msgid ""
#~ "It is now possible to install PCs in UEFI mode instead of using the "
#~ "legacy BIOS emulation."
#~ msgstr ""
#~ "Il est maintenant possible de faire une installation en mode UEFI au lieu "
#~ "d'utiliser l'émulation d'un BIOS classique."

#~ msgid ""
#~ "Asturian, Estonian, Icelandic, Kazakh and Persian have been added to the "
#~ "graphical and text-based installer."
#~ msgstr ""
#~ "l'asturien, l'estonien, l'islandais, le kazakh et le persan qui ont été "
#~ "ajoutés aux installateurs graphique et textuel ;"

#~ msgid ""
#~ "Thai, previously available only in the graphical user interface, is now "
#~ "available also in the text-based installation user interface too."
#~ msgstr ""
#~ "le thaï, jusqu'alors seulement disponible dans l'interface graphique, qui "
#~ "est maintenant aussi disponible dans l'interface d'installation textuelle."

#~ msgid ""
#~ "Due to the lack of translation updates two languages were dropped in this "
#~ "release: Wolof and Welsh."
#~ msgstr ""
#~ "À cause de l'absence de mise à jour de traduction, deux langues ont été "
#~ "abandonnées dans cette version : le wolof et le gallois."

#~ msgid "Dropped platforms"
#~ msgstr "Plateformes abandonnées."

#~ msgid ""
#~ "Support for the Alpha ('alpha'), ARM ('arm') and HP PA-RISC ('hppa')  "
#~ "architectures has been dropped from the installer.  The 'arm' "
#~ "architecture is obsoleted by the ARM EABI ('armel') port."
#~ msgstr ""
#~ "La prise en charge des architectures Alpha (« alpha »), ARM (« arm ») et "
#~ "HP PA-RISC (« hppa ») a été abandonnée dans l'installateur. "
#~ "L'architecture « arm » a été rendue obsolète par le portage ARM EABI "
#~ "(« armel »)."

#~ msgid "Support for kFreeBSD"
#~ msgstr "Prise en charge de kFreeBSD."

#~ msgid ""
#~ "The installer can be used to install the kFreeBSD instead of the Linux "
#~ "kernel and test the technology preview. To use this feature the "
#~ "appropriate installation image (or CD/DVD set) has to be used."
#~ msgstr ""
#~ "L'installateur peut être utilisé pour installer le noyau kFreeBSD au lieu "
#~ "du noyau Linux et tester cette nouveauté technologique. Pour utiliser "
#~ "cette possibilité l'image d'installation appropriée (ou le jeu de CD/DVD "
#~ "approprié) doit être utilisée."

#~ msgid "GRUB 2 is the default bootloader"
#~ msgstr "GRUB 2 est le programme d'amorçage par défaut."

#~ msgid ""
#~ "The bootloader that will be installed by default is <systemitem role="
#~ "\"package\">grub-pc</systemitem> (GRUB 2)."
#~ msgstr ""
#~ "Le programme d'amorçage installé par défaut est <systemitem role=\"package"
#~ "\">grub-pc</systemitem> (GRUB 2)."

#~ msgid ""
#~ "The dialogs presented during the installation process now provide help "
#~ "information. Although not currently used in all dialogs, this feature "
#~ "would be increasingly used in future releases. This will improve the user "
#~ "experience during the installation process, especially for new users."
#~ msgstr ""
#~ "Les boîtes de dialogue affichées pendant la procédure d'installation "
#~ "fournissent maintenant des informations d'aide. Cette fonctionnalité qui "
#~ "n'est pas encore utilisée dans toutes les boîtes de dialogue, sera "
#~ "utilisée de plus en plus dans les futures versions. Cela aidera "
#~ "l'utilisateur lors de la procédure d'installation, notamment les nouveaux "
#~ "utilisateurs."

#~ msgid "Installation of Recommended packages"
#~ msgstr "Installation des paquets recommandés."

#~ msgid ""
#~ "The installation system will install all recommended packages by default "
#~ "throughout the process except for some specific situations in which the "
#~ "general setting gives undesired results."
#~ msgstr ""
#~ "Le système d'installation installera tous les paquets recommandés par "
#~ "défaut durant toute la procédure à l'exception de quelques situations "
#~ "spécifiques où la configuration générale donne des résultats non désirés."

#~ msgid "Automatic installation of hardware-specific packages"
#~ msgstr "Installation automatique de paquets spécifiques au matériel."

#~ msgid ""
#~ "The system will automatically select for installation hardware-specific "
#~ "packages when they are appropriate. This is achieved through the use of "
#~ "<literal>discover-pkginstall</literal> from the <systemitem role=\"package"
#~ "\">discover</systemitem> package."
#~ msgstr ""
#~ "Le système sélectionnera automatiquement pour installation les paquets "
#~ "prévus pour le matériel existant. C'est le rôle du programme "
#~ "<literal>discover-pkginstall</literal> du paquet <systemitem role="
#~ "\"package\">discover</systemitem>."

#~ msgid "Support for installation of previous releases"
#~ msgstr "Prise en charge de l'installation des versions précédentes."

#~ msgid ""
#~ "The installation system can be also used for the installation of previous "
#~ "release, such as &oldreleasename;."
#~ msgstr ""
#~ "Le système d'installation peut aussi être utilisé pour l'installation des "
#~ "versions précédentes, comme &Oldreleasename;."

#~ msgid "Improved mirror selection"
#~ msgstr "Sélection des miroirs améliorée."

#~ msgid ""
#~ "The installation system provides better support for installing both "
#~ "&releasename; as well as &oldreleasename; and older releases (through the "
#~ "use of archive.debian.org). In addition, it will also check that the "
#~ "selected mirror is consistent and holds the selected release."
#~ msgstr ""
#~ "Le système d'installation fournit une meilleure prise en charge pour "
#~ "installer &Releasename; ainsi que &Oldreleasename; et les versions plus "
#~ "anciennes (via l'utilisation de archive.debian.org). De plus, il vérifie "
#~ "que le miroir sélectionné est cohérent et contient la version "
#~ "sélectionnée."

#~ msgid "Changes in partitioning features"
#~ msgstr "Modifications dans les fonctionnalités de partitionnement."

#~ msgid ""
#~ "This release of the installer supports the use of the ext4 file system "
#~ "and it also simplifies the creation of RAID, LVM and crypto protected "
#~ "partitioning systems. Support for the reiserfs file system is no longer "
#~ "included by default, although it can be optionally loaded."
#~ msgstr ""
#~ "Cette version de l'installateur prend en charge l'utilisation du système "
#~ "de fichier ext4. Il simplifie aussi la création de systèmes RAID, LVM et "
#~ "des systèmes de partitions chiffrées. Le système de fichiers ReiserFS "
#~ "n'est plus inclus par défaut, mais il peut être chargé optionnellement."

#~ msgid "Support for loading firmware debs during installation"
#~ msgstr ""
#~ "Chargement de microprogrammes (<quote>firmware</quote>) pendant "
#~ "l'installation."

#~ msgid ""
#~ "It is now possible to load firmware package files from the installation "
#~ "media in addition to removable media, allowing the creation of PXE images "
#~ "and CDs/DVDs with included firmware packages."
#~ msgstr ""
#~ "Il est maintenant possible de charger des microprogrammes depuis le "
#~ "médium d'installation en plus des média amovibles, ce qui permet la "
#~ "création d'images PXE et de CD ou DVD incluant des microprogrammes."

#~ msgid ""
#~ "Starting with Debian &release;, non-free firmware has been moved out of "
#~ "main.  To install Debian on hardware that needs non-free firmware, you "
#~ "can either provide the firmware yourself during installation or use pre-"
#~ "made non-free CDs/DVDs which include the firmware. See the <ulink url="
#~ "\"http://www.debian.org/distrib\">Getting Debian section</ulink> on the "
#~ "Debian website for more information."
#~ msgstr ""
#~ "À partir de Debian &release;, les microprogrammes non libres ont été "
#~ "retirés de la section <quote>main</quote>. Pour installer Debian sur du "
#~ "matériel qui a besoin de microprogrammes non libres, vous pouvez soit "
#~ "fournir le microprogramme vous-même pendant l'installation ou utiliser un "
#~ "CD ou DVD non libre existant qui inclut le microprogramme. Consultez la "
#~ "partie <ulink url=\"http://www.debian.org/distrib\">Récupérer Debian</"
#~ "ulink> du site web de Debian pour obtenir plus de renseignements."

#~ msgid "Improved localisation selection"
#~ msgstr "Amélioration de la sélection de localisation."

#~ msgid ""
#~ "The selection of localisation-related values (language, location and "
#~ "locale settings) is now less interdependent and more flexible. Users will "
#~ "be able to customize the system to their localisation needs more easily "
#~ "while still make it comfortable to use for users that want to select the "
#~ "locale most common for the country they reside in."
#~ msgstr ""
#~ "La sélection des valeurs liées à la localisation (langue, lieu et "
#~ "paramètres régionaux) est maintenant plus souple. Les utilisateurs "
#~ "pourront paramétrer le système selon leurs besoins plus facilement, et "
#~ "l'utilisateur qui veut sélectionner la locale la plus commune pour le "
#~ "pays dans lequel il réside le fera facilement."

#~ msgid ""
#~ "Additionally, the consequences of localisation choices (such as timezone, "
#~ "keymap and mirror selection) are now more obvious to the user."
#~ msgstr ""
#~ "En outre, les conséquences du choix de localisation (comme le fuseau "
#~ "horaire, la configuration du clavier et la sélection du miroir) sont "
#~ "maintenant plus évidentes pour l'utilisateur."

#~ msgid "Live system installation"
#~ msgstr "Installation d'un système autonome (« live »)"

#~ msgid ""
#~ "The installer now supports live systems in two ways. First, an installer "
#~ "included on live system media can use the contents of the live system in "
#~ "place of the regular installation of the base system. Second, the "
#~ "installer may now be launched while running the live system, allowing the "
#~ "user to do other things with the live system during the install. Both "
#~ "features are built into the Debian Live images offered at <ulink url="
#~ "\"http://cdimage.debian.org/\" />."
#~ msgstr ""
#~ "L'installateur prend maintenant en charge les systèmes autonomes de deux "
#~ "manières différentes. Premièrement, un installateur inclus sur le support "
#~ "de système autonome peut utiliser le contenu de ce système autonome à la "
#~ "place de l'installation standard du système de base. Deuxièmement, "
#~ "l'installateur peut maintenant être lancé pendant l'utilisation du "
#~ "système autonome, permettant à l'utilisateur de faire autre chose avec le "
#~ "système autonome pendant l'installation. Ces deux possibilités sont "
#~ "intégrées aux images de Debian Live proposées en <ulink url=\"http://"
#~ "cdimage.debian.org/\" />."
