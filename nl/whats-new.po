# Dutch translation of the Debian release notes.
# Copyright (C) 2012 The Debian Project.
# This file is distributed under the same license as the Debian release notes.
# Jeroen Schot <schot@a-eskwadraat.nl>, 2012.
# Frans Spiesschaert <Frans.Spiesschaert@yucom.be>, 2017, 2019, 2021-2023.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes/whats-new\n"
"POT-Creation-Date: 2023-02-05 13:01+0100\n"
"PO-Revision-Date: 2023-02-06 20:42+0100\n"
"Last-Translator: Frans Spiesschaert <Frans.Spiesschaert@yucom.be>\n"
"Language-Team: Debian Dutch l10n Team <debian-l10n-dutch@lists.debian.org>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.1\n"

#. type: Attribute 'lang' of: <chapter>
#: en/whats-new.dbk:8
msgid "en"
msgstr "nl"

#. type: Content of: <chapter><title>
#: en/whats-new.dbk:9
msgid "What's new in &debian; &release;"
msgstr "Nieuwigheden in &debian; &release;"

#. type: Content of: <chapter><para>
#: en/whats-new.dbk:11
msgid ""
"The <ulink url=\"&url-wiki-newinrelease;\">Wiki</ulink> has more information "
"about this topic."
msgstr ""
"De <ulink url=\"&url-wiki-newinrelease;\">Wiki-pagina</ulink> bevat meer "
"informatie over dit onderwerp."

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:22
msgid "Supported architectures"
msgstr "Ondersteunde architecturen"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:25
msgid ""
"The following are the officially supported architectures for &debian; "
"&release;:"
msgstr ""
"Dit zijn de officieel ondersteunde architecturen voor &debian; &release;:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:31
msgid ""
"32-bit PC (<literal>i386</literal>) and 64-bit PC (<literal>amd64</literal>)"
msgstr ""
"32-bits PC (<literal>i386</literal>) en 64-bits PC (<literal>amd64</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:36
msgid "64-bit ARM (<literal>arm64</literal>)"
msgstr "64-bits ARM (<literal>arm64</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:41
msgid "ARM EABI (<literal>armel</literal>)"
msgstr "ARM EABI (<literal>armel</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:46
msgid "ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)"
msgstr "ARMv7 (EABI hard-float ABI, <literal>armhf</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:51
msgid "little-endian MIPS (<literal>mipsel</literal>)"
msgstr "little-endian MIPS (<literal>mipsel</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:56
msgid "64-bit little-endian MIPS (<literal>mips64el</literal>)"
msgstr "64-bits little-endian MIPS (<literal>mips64el</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:61
msgid "64-bit little-endian PowerPC (<literal>ppc64el</literal>)"
msgstr "64-bits little-endian PowerPC (<literal>ppc64el</literal>)"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:66
msgid "IBM System z (<literal>s390x</literal>)"
msgstr "IBM System z (<literal>s390x</literal>)"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:72
msgid ""
"You can read more about port status, and port-specific information for your "
"architecture at the <ulink url=\"&url-ports;\">Debian port web pages</ulink>."
msgstr ""
"U vindt meer over de status van de voor een bepaalde architectuur geschikt "
"gemaakte versies van Debian (ports genoemd in het taalgebruik van "
"ingewijden) en port-specifieke informatie voor uw architectuur op de <ulink "
"url=\"&url-ports;\">Webpagina's van de Debian ports</ulink>."

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:79
msgid "Archive areas"
msgstr "Archiefgebieden"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:82
msgid ""
"The following archive areas, mentioned in the Social Contract and in the "
"Debian Policy, have been around for a long time:"
msgstr ""
"De volgende archiefgebieden die in het Sociaal Contract en in het Debian "
"Beleidshandboek vermeld worden, bestaan al een lange tijd:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:88
#| msgid "What's new in the distribution?"
msgid "main: the Debian distribution;"
msgstr "main: de Debian-distributie;"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:93
msgid ""
"contrib: supplemental packages intended to work with the Debian "
"distribution, but which require software outside of the distribution to "
"either build or function;"
msgstr ""
"contrib: aanvullende pakketten die bedoeld zijn om met de Debian-distributie "
"te werken, maar waarvoor software buiten de distributie nodig is om gebouwd "
"te worden of te functioneren;"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:100
msgid ""
"non-free: supplemental packages intended to work with the Debian "
"distribution that do not comply with the DFSG or have other problems that "
"make their distribution problematic."
msgstr ""
"non-free: aanvullende pakketten bedoeld zijn om te werken met de Debian-"
"distributie, maar die niet voldoen aan de DFSG, de Debian richtlijnen inzake "
"vrije software, of andere problemen hebben die hun distributie problematisch "
"maken."

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:108
msgid ""
"Following the <ulink url=\"https://www.debian.org/vote/2022/vote_003\">2022 "
"General Resolution about non-free firmware</ulink>, the 5th point of the "
"Social Contract was extended with the following sentence:"
msgstr ""
"Naar aanleiding van de <ulink url=\"https://www.debian.org/vote/2022/"
"vote_003\">Algemene Resolutie van 2022 over niet-vrije firmware</ulink> werd "
"het 5e punt van het Sociaal Contract uitgebreid met de volgende zin:"

#. type: Content of: <chapter><section><blockquote><para>
#: en/whats-new.dbk:114
msgid ""
"The Debian official media may include firmware that is otherwise not part of "
"the Debian system to enable use of Debian with hardware that requires such "
"firmware."
msgstr ""
"De officiële media van Debian kunnen firmware bevatten die anders geen deel "
"uitmaakt van het Debian-systeem, om het gebruik van Debian mogelijk te maken "
"met hardware die dergelijke firmware vereist."

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:121
msgid ""
"While it's not mentioned explicitly in either the Social Contract or Debian "
"Policy yet, a new archive area was introduced, making it possible to "
"separate non-free firmware from the other non-free packages:"
msgstr ""
"Hoewel dit nog niet expliciet wordt vermeld in het Sociaal Contract of in "
"het Debian Beleidshandboek, werd een nieuw archiefgebied ingevoerd, waardoor "
"het mogelijk wordt om niet-vrije firmware te scheiden van de andere niet-"
"vrije pakketten:"

#. type: Content of: <chapter><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:130
msgid "non-free-firmware"
msgstr "non-free-firmware"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:137
msgid ""
"Most non-free firmware packages have been moved from <literal>non-free</"
"literal> to <literal>non-free-firmware</literal> in preparation for the "
"&debian; &release; release.  This clean separation makes it possible to "
"build official installation images with packages from <literal>main</"
"literal> and from <literal>non-free-firmware</literal>, without "
"<literal>contrib</literal> or <literal>non-free</literal>. In turn, these "
"installation images make it possible to install systems with only "
"<literal>main</literal> and <literal>non-free-firmware</literal>, without "
"<literal>contrib</literal> or <literal>non-free</literal>."
msgstr ""
"De meeste niet-vrije firmwarepakketten zijn verplaatst van <literal>non-"
"free</literal> naar <literal>non-free-firmware</literal> ter voorbereiding "
"van de release van &debian; &release;. Deze zuivere scheiding maakt het "
"mogelijk om officiële installatie-images te bouwen met pakketten van "
"<literal>main</literal> en van <literal>non-free-firmware</literal>, zonder "
"<literal>contrib</literal> of <literal>non-free</literal>. Op hun beurt "
"maken deze installatie-images het mogelijk om systemen te installeren met "
"alleen <literal>main</literal> en <literal>non-free-firmware</literal>, "
"zonder <literal>contrib</literal> of <literal>non-free</literal>."

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:147
msgid ""
"See <xref linkend=\"non-free-firmware\"/> for upgrades from &oldreleasename;."
msgstr ""
"Zie <xref linkend=\"non-free-firmware\"/> voor upgrades van &oldreleasename;."

#. type: Content of: <chapter><section><title>
#: en/whats-new.dbk:152
msgid "What's new in the distribution?"
msgstr "Nieuwigheden in de distributie"

#. type: Content of: <chapter><section><programlisting>
#: en/whats-new.dbk:155
#, no-wrap
msgid ""
"      TODO: Make sure you update the numbers in the .ent file\n"
"      using the changes-release.pl script found under ../\n"
"    "
msgstr ""
"      TEDOEN: Zorg ervoor dat u de cijfers in het .ent-bestand bijwerkt\n"
"     met behulp van het script changes-release.pl, te vinden onder ../"

#. type: Content of: <chapter><section><para>
#: en/whats-new.dbk:160
msgid ""
"This new release of Debian again comes with a lot more software than its "
"predecessor &oldreleasename;; the distribution includes over &packages-new; "
"new packages, for a total of over &packages-total; packages.  Most of the "
"software in the distribution has been updated: over &packages-updated; "
"software packages (this is &packages-update-percent;% of all packages in "
"&oldreleasename;).  Also, a significant number of packages (over &packages-"
"removed;, &packages-removed-percent;% of the packages in &oldreleasename;) "
"have for various reasons been removed from the distribution.  You will not "
"see any updates for these packages and they will be marked as \"obsolete\" "
"in package management front-ends; see <xref linkend=\"obsolete\"/>."
msgstr ""
"Deze nieuwe uitgave van Debian bevat opnieuw veel meer software dan zijn "
"voorganger &oldreleasename;; de distributie bevat meer dan &packages-new; "
"nieuwe pakketten, en in totaal meer dan &packages-total; pakketten. De "
"meeste software in de distributie is bijgewerkt: meer dan &packages-updated; "
"softwarepakketten (dit is &packages-update-percent;% van alle pakketten in "
"&oldreleasename;). Er is ook een significant aantal pakketten (meer dan "
"&packages-removed;, &packages-removed-percent;% van de pakketten in "
"&oldreleasename;) verwijderd uit de distributie om diverse redenen. Deze "
"pakketten zullen niet meer worden bijgewerkt en ze zullen als 'achterhaald' "
"of 'verouderd' worden gemarkeerd in de frontends voor pakketbeheer. Zie "
"<xref linkend=\"obsolete\"/>."

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:175
msgid "Desktops and well known packages"
msgstr "Desktops en bekende pakketten"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:177
msgid ""
"&debian; again ships with several desktop applications and environments.  "
"Among others it now includes the desktop environments "
"GNOME<indexterm><primary>GNOME</primary></indexterm> 3.38, KDE "
"Plasma<indexterm><primary>KDE</primary></indexterm> 5.20, "
"LXDE<indexterm><primary>LXDE</primary></indexterm> 11, "
"LXQt<indexterm><primary>LXQt</primary></indexterm> 0.16, "
"MATE<indexterm><primary>MATE</primary></indexterm> 1.24, and "
"Xfce<indexterm><primary>Xfce</primary></indexterm> 4.16."
msgstr ""
"&debian; wordt weer geleverd met verscheidene desktoptoepassingen en -"
"omgevingen. Het bevat nu onder andere de desktopomgevingen "
"GNOME<indexterm><primary>GNOME</primary></indexterm> 3.38, KDE "
"Plasma<indexterm><primary>KDE</primary></indexterm> 5.20, "
"LXDE<indexterm><primary>LXDE</primary></indexterm> 11, "
"LXQt<indexterm><primary>LXQt</primary></indexterm> 0.16, "
"MATE<indexterm><primary>MATE</primary></indexterm> 1.24 en "
"Xfce<indexterm><primary>Xfce</primary></indexterm> 4.16."

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:187
msgid ""
"Productivity applications have also been upgraded, including the office "
"suites:"
msgstr ""
"Ook de productiviteitstoepassingen zijn opgewaardeerd, waaronder de "
"kantoorsoftware:"

#. type: Content of: <chapter><section><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:193
msgid ""
"LibreOffice<indexterm><primary>LibreOffice</primary></indexterm> is upgraded "
"to version 7.0;"
msgstr ""
"LibreOffice<indexterm><primary>LibreOffice</primary></indexterm> werd "
"opgewaardeerd naar versie 7.0;"

#. type: Content of: <chapter><section><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:199
msgid ""
"Calligra<indexterm><primary>Calligra</primary></indexterm> is upgraded to "
"3.2."
msgstr ""
"Calligra<indexterm><primary>Calligra</primary></indexterm> werd "
"opgewaardeerd naar 3.2."

#. type: Content of: <chapter><section><section><itemizedlist><listitem><para>
#: en/whats-new.dbk:205
msgid ""
"GNUcash<indexterm><primary>GNUcash</primary></indexterm> is upgraded to 4.4;"
msgstr ""
"GNUcash<indexterm><primary>GNUcash</primary></indexterm> werd opgewaardeerd "
"naar 4.4;"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:226
msgid ""
"Among many others, this release also includes the following software updates:"
msgstr ""
"Deze uitgave bevat daarnaast onder meer de volgende bijgewerkte software:"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:236
msgid "Package"
msgstr "Pakket"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:237
msgid "Version in &oldrelease; (&oldreleasename;)"
msgstr "Versie in &oldrelease; (&oldreleasename;)"

#. type: Content of: <chapter><section><section><informaltable><tgroup><thead><row><entry>
#: en/whats-new.dbk:238
msgid "Version in &release; (&releasename;)"
msgstr "Versie in &release; (&releasename;)"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:243
msgid "Apache<indexterm><primary>Apache</primary></indexterm>"
msgstr "Apache<indexterm><primary>Apache</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:244
msgid "2.4.38"
msgstr "2.4.38"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:245
msgid "2.4.48"
msgstr "2.4.48"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:248
msgid ""
"BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym> "
"Server"
msgstr ""
"BIND<indexterm><primary>BIND</primary></indexterm> <acronym>DNS</acronym>-"
"server"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:249
msgid "9.11"
msgstr "9.11"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:250
msgid "9.16"
msgstr "9.16"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:265
msgid "Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm>"
msgstr "Cryptsetup<indexterm><primary>Cryptsetup</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:266
msgid "2.1"
msgstr "2.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:267
msgid "2.3"
msgstr "2.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:277
msgid ""
"Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</"
"acronym>"
msgstr ""
"Dovecot<indexterm><primary>Dovecot</primary></indexterm> <acronym>MTA</"
"acronym>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:278
msgid "2.3.4"
msgstr "2.3.4"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:279
msgid "2.3.13"
msgstr "2.3.13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:282
msgid "Emacs"
msgstr "Emacs"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:283
msgid "26.1"
msgstr "26.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:284
msgid "27.1"
msgstr "27.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:287
msgid ""
"Exim<indexterm><primary>Exim</primary></indexterm> default e-mail server"
msgstr ""
"Exim<indexterm><primary>Exim</primary></indexterm> standaard e-mailserver"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:288
msgid "4.92"
msgstr "4.92"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:289
msgid "4.94"
msgstr "4.94"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:299
msgid ""
"<acronym>GNU</acronym> Compiler Collection as default "
"compiler<indexterm><primary>GCC</primary></indexterm>"
msgstr ""
"<acronym>GNU</acronym> Compiler Collection als standaard-"
"compiler<indexterm><primary>GCC</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:300
msgid "8.3"
msgstr "8.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:301
msgid "10.2"
msgstr "10.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:304
msgid "<acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm>"
msgstr "<acronym>GIMP</acronym><indexterm><primary>GIMP</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:305
msgid "2.10.8"
msgstr "2.10.8"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:306
msgid "2.10.22"
msgstr "2.10.22"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:309
msgid "GnuPG<indexterm><primary>GnuPG</primary></indexterm>"
msgstr "GnuPG<indexterm><primary>GnuPG</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:310
msgid "2.2.12"
msgstr "2.2.12"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:311
msgid "2.2.27"
msgstr "2.2.27"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:314
msgid "Inkscape<indexterm><primary>Inkscape</primary></indexterm>"
msgstr "Inkscape<indexterm><primary>Inkscape</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:315
msgid "0.92.4"
msgstr "0.92.4"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:316
msgid "1.0.2"
msgstr "1.0.2"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:319
msgid "the <acronym>GNU</acronym> C library"
msgstr "de <acronym>GNU</acronym> C-bibliotheek"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:320
msgid "2.28"
msgstr "2.28"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:321
msgid "2.31"
msgstr "2.31"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:324
msgid "lighttpd"
msgstr "lighttpd"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:325
msgid "1.4.53"
msgstr "1.4.53"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:326
msgid "1.4.59"
msgstr "1.4.59"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:329
msgid "Linux kernel image"
msgstr "Linux kernel-image"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:330
msgid "4.19 series"
msgstr "4.19-serie"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:331
msgid "5.10 series"
msgstr "5.10-serie"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:334
msgid "LLVM/Clang toolchain"
msgstr "LLVM/Clang-gereedschapsset"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:335
msgid "6.0.1 and 7.0.1 (default)"
msgstr "6.0.1 en 7.0.1 (standaard)"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:336
msgid "9.0.1 and 11.0.1 (default)"
msgstr "9.0.1 en 11.0.1 (standaard)"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:339
msgid "MariaDB<indexterm><primary>MariaDB</primary></indexterm>"
msgstr "MariaDB<indexterm><primary>MariaDB</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:340
msgid "10.3"
msgstr "10.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:341
msgid "10.5"
msgstr "10.5"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:344
msgid "Nginx<indexterm><primary>Nginx</primary></indexterm>"
msgstr "Nginx<indexterm><primary>Nginx</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:345
msgid "1.14"
msgstr "1.14"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:346
msgid "1.18"
msgstr "1.18"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:356
msgid "OpenJDK<indexterm><primary>OpenJDK</primary></indexterm>"
msgstr "OpenJDK<indexterm><primary>OpenJDK</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:357 en/whats-new.dbk:358 en/whats-new.dbk:382
msgid "11"
msgstr "11"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:361
msgid "OpenSSH<indexterm><primary>OpenSSH</primary></indexterm>"
msgstr "OpenSSH<indexterm><primary>OpenSSH</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:362
msgid "7.9p1"
msgstr "7.9p1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:363
msgid "8.4p1"
msgstr "8.4p1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:366
msgid "Perl<indexterm><primary>Perl</primary></indexterm>"
msgstr "Perl<indexterm><primary>Perl</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:367
msgid "5.28"
msgstr "5.28"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:368
msgid "5.32"
msgstr "5.32"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:371
msgid "<acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm>"
msgstr "<acronym>PHP</acronym><indexterm><primary>PHP</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:372
msgid "7.3"
msgstr "7.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:373
msgid "7.4"
msgstr "7.4"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:376
msgid ""
"Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</"
"acronym>"
msgstr ""
"Postfix<indexterm><primary>Postfix</primary></indexterm> <acronym>MTA</"
"acronym>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:377
msgid "3.4"
msgstr "3.4"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:378
msgid "3.5"
msgstr "3.5"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:381
msgid "PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm>"
msgstr "PostgreSQL<indexterm><primary>PostgreSQL</primary></indexterm>"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:383
msgid "13"
msgstr "13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:393
msgid "Python 3"
msgstr "Python 3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:394
msgid "3.7.3"
msgstr "3.7.3"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:395
msgid "3.9.1"
msgstr "3.9.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:398
msgid "Rustc"
msgstr "Rustc"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:399
msgid "1.41 (1.34 for <literal>armel</literal>)"
msgstr "1.41 (1.34 voor <literal>armel</literal>)"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:400
msgid "1.48"
msgstr "1.48"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:403
msgid "Samba"
msgstr "Samba"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:404
msgid "4.9"
msgstr "4.9"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:405
msgid "4.13"
msgstr "4.13"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:408
msgid "Vim"
msgstr "Vim"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:409
msgid "8.1"
msgstr "8.1"

#. type: Content of: <chapter><section><section><informaltable><tgroup><tbody><row><entry>
#: en/whats-new.dbk:410
msgid "8.2"
msgstr "8.2"

#. type: Content of: <chapter><section><section><title>
#: en/whats-new.dbk:418
msgid "Something"
msgstr "Iets"

#. type: Content of: <chapter><section><section><para>
#: en/whats-new.dbk:420
msgid "Text"
msgstr "Tekst"
