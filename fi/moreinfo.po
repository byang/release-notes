# Tapio Lehtonen <tale@debian.org>, 2005, 2007, 2009.
# Tommi Vainikainen <thv@iki.fi>, 2006-2007.
#
msgid ""
msgstr ""
"Project-Id-Version: release-notes\n"
"POT-Creation-Date: 2021-05-04 19:18+0200\n"
"PO-Revision-Date: 2009-09-16 13:50+0300\n"
"Last-Translator: Tapio Lehtonen <tale@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. type: Attribute 'lang' of: <chapter>
#: en/moreinfo.dbk:8
msgid "en"
msgstr "fi"

#. type: Content of: <chapter><title>
#: en/moreinfo.dbk:9
msgid "More information on &debian;"
msgstr "Lisätietoja käyttöjärjestelmästä &debian;"

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:11
msgid "Further reading"
msgstr "Lisää lukemista"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:13
#, fuzzy
#| msgid ""
#| "Beyond these release notes and the installation guide, further "
#| "documentation on &debian; is available from the Debian Documentation "
#| "Project (DDP), whose goal is to create high-quality documentation for "
#| "Debian users and developers.  Documentation, including the Debian "
#| "Reference, Debian New Maintainers Guide, and Debian FAQ are available, "
#| "and many more.  For full details of the existing resources see the <ulink "
#| "url=\"&url-ddp;\">DDP website</ulink>."
msgid ""
"Beyond these release notes and the installation guide, further documentation "
"on Debian is available from the Debian Documentation Project (DDP), whose "
"goal is to create high-quality documentation for Debian users and "
"developers, such as the Debian Reference, Debian New Maintainers Guide, the "
"Debian FAQ, and many more.  For full details of the existing resources see "
"the <ulink url=\"&url-ddp;\">Debian Documentation website</ulink> and the "
"<ulink url=\"&url-wiki;\">Debian Wiki</ulink>."
msgstr ""
"Tämän julkaisumuistion ja asennusoppaan lukemisen jälkeen &debian; "
"järjestelmän opiskeluun on tarjolla Debian-Dokumentointiprojekti (DDP). Sen "
"tarkoituksena on luoda korkealaatuista ohjeistusta Debianin käyttäjille ja "
"kehittäjille. Saatavilla olevia ohjeita ovat Debian Reference, Debian New "
"Maintainers Guide ja Debian FAQ, sekä useita muita. Yksityiskohtaiset tiedot "
"saatavilla olevasta aineistosta löytyvät <ulink url=\"&url-ddp;\">DDP:n "
"seittisivustosta</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:23
#, fuzzy
#| msgid ""
#| "Documentation for individual packages is installed into <filename>/usr/"
#| "share/doc/<replaceable>package</replaceable></filename>.  This may "
#| "include copyright information, Debian specific details and any upstream "
#| "documentation."
msgid ""
"Documentation for individual packages is installed into <filename>/usr/share/"
"doc/<replaceable>package</replaceable></filename>.  This may include "
"copyright information, Debian specific details, and any upstream "
"documentation."
msgstr ""
"Yksittäisten pakettien ohjeistus on asennettu hakemistoon <filename>/usr/"
"share/doc/<replaceable>paketti</replaceable></filename>, tähän voi kuulua "
"tekijänoikeustietoa, Debianiin liittyviä yksityiskohtia ja alkuperäisen "
"tekijän ohjeistus."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:31
msgid "Getting help"
msgstr "Apua löytyy"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:33
#, fuzzy
#| msgid ""
#| "There are many sources of help, advice and support for Debian users, but "
#| "these should only be considered if research into documentation of the "
#| "issue has exhausted all sources.  This section provides a short "
#| "introduction into these which may be helpful for new Debian users."
msgid ""
"There are many sources of help, advice, and support for Debian users, though "
"these should only be considered after researching the issue in available "
"documentation.  This section provides a short introduction to these sources "
"which may be helpful for new Debian users."
msgstr ""
"Apua, neuvoja ja tukea Debianin käyttäjille on saatavilla monelta taholta, "
"mutta näitä olisi käytettävä vasta kun aiheesta saatavilla olevan "
"ohjeistuksen huolellinen tutkiminen ei ole auttanut. Tässä luvussa kerrotaan "
"johdannonomaisesti uusille Debianin käyttäjille mahdollisesti hyödyllisistä "
"välineistä."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:39
msgid "Mailing lists"
msgstr "Postilistat"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:41
msgid ""
"The mailing lists of most interest to Debian users are the debian-user list "
"(English) and other debian-user-<replaceable>language</replaceable> lists "
"(for other languages).  For information on these lists and details of how to "
"subscribe see <ulink url=\"&url-debian-list-archives;\"></ulink>.  Please "
"check the archives for answers to your question prior to posting and also "
"adhere to standard list etiquette."
msgstr ""
"Debianin käyttäjille kiinnostavimmat sähköpostilistat ovat debian-user "
"(englanninkielinen) ja muut debian-user-<replaceable>kieli</replaceable>-"
"listat (useille muille kielille). Tietoa näistä postituslistoista ja ohjeet "
"listalle liittymiseen löytyy osoitteesta <ulink url=\"&url-debian-list-"
"archives;\"></ulink>. Ole hyvä ja tarkista listojen arkistoista onko "
"kysymykseesi jo vastattu ennen kuin lähetät kysymyksesi listalle. Noudata "
"myös tavanomaisia <ulink url=\"http://www.cs.tut.fi/~jkorpela/jakelulistat."
"html\">sähköpostilistojen käyttäytymissääntöjä</ulink>."

#. type: Content of: <chapter><section><section><title>
#: en/moreinfo.dbk:51
msgid "Internet Relay Chat"
msgstr "IRC"

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:53
#, fuzzy
#| msgid ""
#| "Debian has an IRC channel dedicated to the support and aid of Debian "
#| "users located on the OFTC IRC network.  To access the channel, point your "
#| "favorite IRC client at irc.debian.org and join <literal>#debian</literal>."
msgid ""
"Debian has an IRC channel dedicated to support and aid for Debian users, "
"located on the OFTC IRC network.  To access the channel, point your favorite "
"IRC client at irc.debian.org and join <literal>#debian</literal>."
msgstr ""
"Debianilla on käyttäjien tukeen ja auttamiseen tarkoitettu IRC-kanava OFTC-"
"IRC-verkossa. Pääset kanavalle menemällä haluamallasi IRC-asiakasohjelmalla "
"palvelimelle irc.debian.org ja liittymällä kanavalle <literal>#debian</"
"literal>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:58
#, fuzzy
#| msgid ""
#| "Please follow the channel guidelines, respecting other users fully.  The "
#| "guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
#| "Wiki</ulink>."
msgid ""
"Please follow the channel guidelines, respecting other users fully.  The "
"guidelines are available at the <ulink url=\"&url-wiki;DebianIRC\">Debian "
"Wiki</ulink>."
msgstr ""
"Noudata kanavan ohjeistusta ja kunnioita muiden käyttäjien mielipiteitä. "
"Ohjeistus on luettavissa <ulink url=\"&url-wiki;DebianIRC\">Debian Wikissä</"
"ulink>."

#. type: Content of: <chapter><section><section><para>
#: en/moreinfo.dbk:63
msgid ""
"For more information on OFTC please visit the <ulink url=\"&url-irc-host;"
"\">website</ulink>."
msgstr ""
"Lisää tietoa OFTC:stä löytyy projektin <ulink url=\"&url-irc-host;"
"\">seittisivustolta</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:71
msgid "Reporting bugs"
msgstr "Vioista ilmoittaminen"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:73
#, fuzzy
#| msgid ""
#| "We strive to make &debian; a high quality operating system, however that "
#| "does not mean that the packages we provide are totally free of bugs.  "
#| "Consistent with Debian's <quote>open development</quote> philosophy and "
#| "as a service to our users, we provide all the information on reported "
#| "bugs at our own Bug Tracking System (BTS).  The BTS is browseable at "
#| "<ulink url=\"&url-bts;\"></ulink>."
msgid ""
"We strive to make Debian a high-quality operating system; however that does "
"not mean that the packages we provide are totally free of bugs.  Consistent "
"with Debian's <quote>open development</quote> philosophy and as a service to "
"our users, we provide all the information on reported bugs at our own Bug "
"Tracking System (BTS).  The BTS can be browsed at <ulink url=\"&url-bts;\"></"
"ulink>."
msgstr ""
"Ponnistelemme, jotta &debian; olisi korkealaatuinen käyttöjärjestelmä, mikä "
"ei kuitenkaan tarkoita tekemiemme ohjelmapakettien olevan kokonaan vailla "
"vikoja. Julkinen vikatietokanta sopii Debianin <quote>avoimeen "
"kehitystyöhön</quote>. Palvelemme käyttäjiämme tarjoamalla kaikki tiedot "
"ilmoitetuista vioista omassa vianseurantajärjestelmässämme (BTS, Bug "
"Tracking System), joka on käytettävissä selaimella osoitteessa <ulink url="
"\"&url-bts;\"></ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:81
#, fuzzy
#| msgid ""
#| "If you find a bug in the distribution or in packaged software that is "
#| "part of it, please report it so that it can be properly fixed for future "
#| "releases.  Reporting bugs requires a valid email address.  We ask for "
#| "this so that we can trace bugs and developers can get in contact with "
#| "submitters should additional information be needed."
msgid ""
"If you find a bug in the distribution or in packaged software that is part "
"of it, please report it so that it can be properly fixed for future "
"releases.  Reporting bugs requires a valid e-mail address.  We ask for this "
"so that we can trace bugs and developers can get in contact with submitters "
"should additional information be needed."
msgstr ""
"Jos löydät vian jakelusta tai sen osana olevasta paketoidusta ohjelmasta, "
"ole hyvä ja ilmoita siitä, jotta se saadaan kunnolla korjattua seuraavissa "
"versioissa. Vikailmoituksen laatijalla on oltava toimiva sähköpostiosoite. "
"Tämä siksi, että vikoja voidaan seurata ja kehittävät voivat tarvittaessa "
"pyytää lisätietoja."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:88
#, fuzzy
#| msgid ""
#| "You can submit a bug report using the program <command>reportbug</"
#| "command> or manually using email.  You can read more about the Bug "
#| "Tracking System and how to use it by reading the reference documentation "
#| "(available at <filename>/usr/share/doc/debian</filename> if you have "
#| "<systemitem role=\"package\">doc-debian</systemitem> installed) or online "
#| "at the <ulink url=\"&url-bts;\">Bug Tracking System</ulink>."
msgid ""
"You can submit a bug report using the program <command>reportbug</command> "
"or manually using e-mail.  You can find out more about the Bug Tracking "
"System and how to use it by reading the reference documentation (available "
"at <filename>/usr/share/doc/debian</filename> if you have <systemitem role="
"\"package\">doc-debian</systemitem> installed) or online at the <ulink url="
"\"&url-bts;\">Bug Tracking System</ulink>."
msgstr ""
"Vikailmoitus voidaan lähettää ohjelmalla <command>reportbug</command> tai "
"lähettämällä suoraan sähköpostia. Lisää tietoja vianseurantajärjestelmästä "
"ja sen käyttöohjeita voi lukea hakemiston <filename>/usr/share/doc/debian</"
"filename> ohjetiedostoista mikäli <systemitem role=\"package\">doc-debian</"
"systemitem> on asennettu tai netistä osoitteesta <ulink url=\"&url-bts;"
"\">Vianseurantajärjestelmä</ulink>."

#. type: Content of: <chapter><section><title>
#: en/moreinfo.dbk:98
msgid "Contributing to Debian"
msgstr "Debianin avustaminen"

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:100
#, fuzzy
#| msgid ""
#| "You do not need to be an expert to contribute to Debian.  By assisting "
#| "users with problems on the various user support <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> you are contributing to the community.  "
#| "Identifying (and also solving) problems related to the development of the "
#| "distribution by participating on the development <ulink url=\"&url-debian-"
#| "list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
#| "Debian's high quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
#| "ulink> and help developers track them down and fix them.  If you have a "
#| "way with words then you may want to contribute more actively by helping "
#| "to write <ulink url=\"&url-ddp;\">documentation</ulink> or <ulink url="
#| "\"&url-debian-i18n;\">translate</ulink> existing documentation into your "
#| "own language."
msgid ""
"You do not need to be an expert to contribute to Debian.  By assisting users "
"with problems on the various user support <ulink url=\"&url-debian-list-"
"archives;\">lists</ulink> you are contributing to the community.  "
"Identifying (and also solving) problems related to the development of the "
"distribution by participating on the development <ulink url=\"&url-debian-"
"list-archives;\">lists</ulink> is also extremely helpful.  To maintain "
"Debian's high-quality distribution, <ulink url=\"&url-bts;\">submit bugs</"
"ulink> and help developers track them down and fix them.  The tool "
"<systemitem role=\"package\">how-can-i-help</systemitem> helps you to find "
"suitable reported bugs to work on.  If you have a way with words then you "
"may want to contribute more actively by helping to write <ulink url=\"&url-"
"ddp-vcs-info;\">documentation</ulink> or <ulink url=\"&url-debian-i18n;"
"\">translate</ulink> existing documentation into your own language."
msgstr ""
"Debianin avustamiseksi ei tarvitse olla asiantuntija. Auttamalla käyttäjiä "
"selvittämään ongelmatilanteet <url id=\"&url-debian-list-archives;\" name="
"\"postilistoilla\"> tuet yhteisöä. Jakelun kehittämiseen liittyvien "
"ongelmien tunnistaminen (ja mikä tärkeintä ratkaiseminen) osallistumalla "
"kehittäjien <url id=\"&url-debian-list-archives;\" name=\"postilistojen\"> "
"keskusteluun on myös erittäin hyödyllistä. Auta Debiania säilyttämään laatu "
"hyvänä <url id=\"&url-bts;\" name=\"lähettämällä vikailmoituksia\"> ja auta "
"kehittäjiä löytämään ja korjaamaan viat. Jos kirjoittaminen sujuu, haluaisit "
"ehkä avustaa aktiivisemmin auttamalla <url id=\"&url-ddp;\" name=\"ohjeiden"
"\"> kirjoittamisessa tai <url id=\"&url-debian-i18n;\" name=\"kääntämällä\"> "
"jo tehtyjä ohjeita äidinkielellesi."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:117
#, fuzzy
#| msgid ""
#| "If you can dedicate more time, you could manage a piece of the Free "
#| "Software collection within Debian.  Especially helpful is if people adopt "
#| "or maintain items that people have requested for inclusion within "
#| "Debian.  The <ulink url=\"&url-wnpp;\">Work Needing and Prospective "
#| "Packages database</ulink> details this information.  If you have an "
#| "interest in specific groups then you may find enjoyment in contributing "
#| "to some of Debian's subprojects which include ports to particular "
#| "architectures, <ulink url=\"&url-debian-jr;\">Debian Jr.</ulink> and "
#| "<ulink url=\"&url-debian-med;\">Debian Med</ulink>."
msgid ""
"If you can dedicate more time, you could manage a piece of the Free Software "
"collection within Debian.  Especially helpful is if people adopt or maintain "
"items that people have requested for inclusion within Debian.  The <ulink "
"url=\"&url-wnpp;\">Work Needing and Prospective Packages database</ulink> "
"details this information.  If you have an interest in specific groups then "
"you may find enjoyment in contributing to some of Debian's <ulink url=\"&url-"
"debian-projects;\">subprojects</ulink> which include ports to particular "
"architectures and <ulink url=\"&url-debian-blends;\">Debian Pure Blends</"
"ulink> for specific user groups, among many others."
msgstr ""
"Jos voit panostaa enemmän aikaa, voit ylläpitää vapaiden ohjelmien kokoelman "
"osaa Debianissa. Erityisen hyödyllistä on ottaa vastuulleen tai ylläpitää "
"toivottuja lisäyksiä Debianiin, seittisivusto <ulink url=\"&url-wnpp;\">Apua "
"tarvitsevat paketit</ulink> tarjoaa tästä yksityiskohtaisempaa tietoa. Jos "
"mielenkiintosi kohteina ovat tietyt kohderyhmät, saatat ilahtua voidessasi "
"avustaa Debianin osaprojekteja kuten <ulink url=\"&url-debian-jr;\">Debian "
"Jr.</ulink> ja <ulink url=\"&url-debian-med;\">Debian Med</ulink>."

#. type: Content of: <chapter><section><para>
#: en/moreinfo.dbk:128
#, fuzzy
#| msgid ""
#| "In any case, if you are working in the free software community in any "
#| "way, as a user, programmer, writer or translator you are already helping "
#| "the free software effort.  Contributing is rewarding and fun, and as well "
#| "as allowing you to meet new people it gives you that warm fuzzy feeling "
#| "inside."
msgid ""
"In any case, if you are working in the free software community in any way, "
"as a user, programmer, writer, or translator you are already helping the "
"free software effort.  Contributing is rewarding and fun, and as well as "
"allowing you to meet new people it gives you that warm fuzzy feeling inside."
msgstr ""
"Jos työskentelet vapaiden ohjelmien yhteisössä millä tahansa tavalla, "
"käyttäjänä, ohjelmoijana, kirjailijana tai kääntäjänä, olet jo tukemassa "
"vapaiden ohjelmien aatetta. Avustaminen on palkitsevaa ja hauskaa sekä "
"auttaa tutustumaan uusiin kavereihin ja siitä tulee hyvä olo."
